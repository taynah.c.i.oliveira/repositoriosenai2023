const {createApp}= Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
        };//Fechamento return
   },//Fechamento data
    methods:{
        getNumero(numero){
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                // this.valorDisplay = this.valorDisplay + numero.toString();
                //adição simplificada
                this.valorDisplay += numero.toString();
            }
        },//Fechamento getNumero

        limpar(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
        },//Fechamento limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){
            this.valorDisplay += "."
            }//Fechamento if
        },//Fechamento decimal

        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case "+":
                            this.valorDisplay =(this.numeroAtual + displayAtual).toString();
                            break;
                        case "-":
                            this.valorDisplay =(this.numeroAtual - displayAtual).toString();
                            break;
                        case "*":
                            this.valorDisplay =(this.numeroAtual * displayAtual).toString();
                            break;
                        case "/":
                            this.valorDisplay =(this.numeroAtual / displayAtual).toString();
                            break;

                    }//fim do switch
                    this.numeroAnterior = this.numeroAtual;
                    this.numeroAtual = null;
                    this.operador = null;
                }//fim da if
                else{
                    this.numeroAnterior= displayAtual;
                }//fim do else
            }//fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
            this.valorDisplay = "0";
            }

        }// fim das operações
    },//Fechamento methods

}).mount("#app");//Fechamento app