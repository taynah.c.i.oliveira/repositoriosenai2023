const {createApp} = Vue;

createApp({
    data(){
        return{
            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,
        }//fechamento return
    },//fechamento data

    methods:{
        login(){
            // alert("testando...");

            //simulando uma requisição de Login assincrona
            setTimeout(() =>{
                if((this.usuario ==="Taynah" && this.senha === "12345678") || 
                (this.usuario ==="Tayna" && this.senha === "123456")){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";
                    // alert("Login efetuado com sucesso!");
                }// Fim da if
                else{
                    // alert("Usuário ou senha incorretos!");
                    this.erro = "Usuário ou senha incorretos!";
                    this.sucesso = null;

                }
            }, 1000);
        }, //fechamento login
    }, //fechamento methods

}).mount("#app");//Fechamento app